const APIs = {
  channel: 'WEBBOOKING',
  responses: {
    OK: {
      code: 0,
      message: 'OK',
    },
    TOKEN_INVALID: {
      code: 3,
      message: 'Token không hợp lệ',
    },
    TOKEN_EXPIRE: {
      code: 2,
      message: 'Token đã hết hạn',
    },
    '-1': {
      name: 'UNKNOWN',
      message: 'Lỗi không xác định',
    },
    0: {
      name: 'SUCCESS',
      message: 'Thành công',
    },
    1: {
      name: 'TOKEN_INVALID',
      message: 'Token không hợp lệ',
    },
    2: {
      name: 'TOKEN_EXPIRE',
      message: 'Token đã hết hạn',
    },
    3: {
      name: 'REQUEST_INVALID',
      message: 'Yêu cầu không hợp lệ',
    },
    4: {
      name: 'SYSTEM_BUSY',
      message: 'Tên tài khoản hoắc mật khẩu sai',
    },
    5: {
      name: 'UNKNOWN_ERROR',
      message: 'Lỗi không xác định',
    },
    6: {
      name: 'PHONE_INVALID',
      message: 'Sản phẩm không hợp lệ',
    },
    7: {
      name: 'MAIL_INVALID',
      message: 'Email không hợp lệ',
    },
    8: {
      name: 'EMAIL_NOT_YES',
      message: 'Email không được để trống',
    },
    9: {
      name: 'EMAIL_NOT_YES_AND_PHONE_INVALID',
      message: 'Số điện thoại không hợp lệ, Email không được để trống',
    },
    // '10': {
    //   name: 'MAIL_INVALID_AND_PHONE_INVALID',
    //   message: 'Số điện thoại không hợp lệ, Email không hợp lệ'
    // },
    10: {
      name: 'POINT_EXCEED',
      message: 'Tổng tỷ trọng điểm không được lớn hơn 100',
    },
    11: {
      name: 'CHANEL_RECEIVE_NOTIFICATION_INVALID',
      message: 'Kênh nhận thông báo cước không hợp lệ',
    },
    12: {
      name: 'CHANEL_CANCEL_NOTIFICATION_INVALID',
      message: 'Kênh hủy thông báo cước không hợp lệ',
    },
    13: {
      name: 'ERROR_RE_EXPORT_RELEASE',
      message: 'Bạn đã nhập sai thông tin xuất lại TBC.',
    },
    14: {
      name: 'ERROR_RECEIVER_RELEASE_EMAIL',
      message: 'Bạn đã nhập sai thông tin nhận TBC qua Email.',
    },
    15: {
      name: 'ERROR_RECEIVER_RELEASE_SMS',
      message: 'Bạn đã nhập sai thông tin nhận TBC qua SMS.',
    },
    16: {
      name: 'CYCLE_INVALID',
      message: 'Chu kỳ không hợp lệ.',
    },
    17: {
      name: 'MONTH_INVALID',
      message: 'Tháng không hợp lệ.',
    },
    18: {
      name: 'DATA_INVALID',
      message: 'Dữ liệu hợp lệ.',
    },
    99: {
      name: 'DATA_INVALID',
      message: 'Lỗi hệ thống',
    },
    999: {
      name: 'SYSTEM_ERROR',
      message: 'Lỗi hệ thống',
    },
    1004: {
      name: 'SYSTEM_ERROR',
      message: 'Tên đăng nhập hoặc mật khẩu không chính xác',
    },
    1010: {
      name: 'ACCOUNT_IS_ADMIN',
      message: 'Không thể xóa tài khoản quản trị viên',
    },
    1101: {
      name: 'IP_EXIST',
      message: 'Địa chỉ IP đã tồn tại.',
    },
    1102: {
      name: 'SCHEDULE_EXIST',
      message: 'Lịch truy cập đã tồn tại.',
    },
    1103: {
      name: 'SCHEDULE_DETAIL_EXIST',
      message: 'Lịch truy cập chi tiết đã tồn tại.',
    },
    1104: {
      name: 'ACTION_INVALID',
      message: 'Hành động không đúng.',
    },
    1105: {
      name: 'ACCOUNT_LOCKED',
      message: 'Tài khoản đã bị khóa.',
    },
    1106: {
      name: 'ACCOUNT_OR_PASSWORD_INCORECT',
      message: 'Tài khoản hoặc mật khẩu sai.',
    },
    1107: {
      name: 'ACCOUNT_EXIST',
      message: 'Tài khoản đã tồn tại.',
    },
    1108: {
      code: 1108,
      name: 'USER_NOT_EXIST',
      message: 'Tài khoản này không tồn tại.',
    },
    1109: {
      name: 'PASSWORD_INCORECT',
      message: 'Mật khẩu không đúng.',
    },
    1110: {
      name: 'ACCOUNT_IS_ADMIN',
      message: 'Tài khoản là Admin.',
    },
    1014: {
      name: 'ACCOUNT_IS_ADMIN',
      message: 'Mật khẩu không chính xác',
    },
    1111: {
      name: 'GROUP_IS_EXIST',
      message: 'Nhóm đã tồn tại.',
    },

    1112: {
      name: 'GROUP_NOT_EXIST',
      message: 'Nhóm không tồn tại.',
    },
    1113: {
      name: 'THE_GROUP_HAS_USERS',
      message: 'Nhóm đang có người dùng.',
    },
    1114: {
      name: 'GROUPS_NOT_EXIST',
      message: 'Nhóm không tồn tại.',
    },
    1115: {
      name: 'IP_ADDRESS_ACCESS_DENIED',
      message:
        'IP của bạn đã bị chặn quyền truy cập. Vui lòng liên hệ quản trị viên để biết thêm thông tin',
    },
    1116: {
      code: 1116,
      name: 'SCHEDULE_ACCESS_DENIED',
      message: 'Lịch truy cập của bạn không được cấp tại thời điểm này.',
    },
    1140: {
      name: 'THE_SCHEDULE_HAS_USER ',
      message: 'Lịch truy cập đang được áp dụng cho tài khoản người dùng.',
    },
    1141: {
      name: 'THE_SCHEDULE_HAS_CYCLE ',
      message: 'Lịch truy cập đang được áp dụng cho chu kỳ cước.',
    },
    1183: {
      name: 'URL_EXPIRE ',
      message: 'URL hết hiệu lực.',
    },
    1184: {
      name: 'IS_LDAP_USER ',
      message: 'Tài khoản là người dùng LDAP.',
    },
    1189: {
      name: 'TEMPLATE_EMAIL_TIME_EXSIST',
      message: 'Thời gian cấu hình mail đã tồn tại.',
    },
    1190: {
      name: 'CYCLE_IS_EXPORTING ',
      message: 'Chu kỳ đang xuất cước.',
    },
    1200: {
      name: 'USER_CHECKED_OUT',
      message: 'USER CHECKED OUT',
    },
    1201: {
      name: 'USER_CHECKED_IN',
      message: 'USER CHECKED IN',
    },
    1202: {
      name: 'PRODUCTIVITY_PLAN_NOT_EXIST',
      message: 'Kế hoạch không tồn tại',
    },
    1203: {
      name: 'STATUS_CANT_UPDATED',
      message: 'Không thể cập nhật trạng thái',
    },
    1204: {
      name: 'DEPARTMENT_NOT_EXIST',
      message: 'Bộ phận không tồn tại',
    },
    1205: {
      name: 'TEMPLATE_USED',
      message: 'Mẫu đã được sử dụng',
    },
    1206: {
      name: 'END_TIME_IS_LESS_THEN_CURRENT_TIME',
      message: 'Thời gian kết thúc thấp hơn thời gian hiện tại',
    },
    1207: {
      name: 'Lịch truy cập người dùng không tồn tại',
      message: '',
    },
    1208: {
      name: 'USER_NO_NEED_CHECK_OUT',
      message: 'USER NO NEED CHECK OUT ',
    },
    1209: {
      name: 'TIMEKEEPING_DISTANCE_INVALID',
      message: 'Khoảng cách thời gian không hợp lệ',
    },
    1210: {
      name: 'The account is already in another group !',
      message: 'Tài khoản đã thuộc nhóm khác',
    },
    1211: {
      name: 'PLAN_APPROVED',
      message: 'Kế hoạch đã được phê duyệt',
    },
    1212: {
      name: 'PLAN_EXPIRED',
      message: 'Đã quá thời gian duyệt kế hoạch',
    },
    1213: {
      name: 'ACTION_DETAIL_VALUE_APP_NOT_EXIST',
      message: 'ACTION DETAIL VALUE APP NOT EXIST',
    },
    1214: {
      name: 'USER_NOT_ALLOW',
      message: 'Người dùng không được phép',
    },
    1215: {
      name: 'PLAN_LOOKED',
      message: 'Kế hoạch đã bị khóa',
    },
    1216: {
      name: 'PLAN_STATUS_NOT_ALLOW',
      message: 'Bạn không có quyền gửi yêu cầu duyệt',
    },
    1217: {
      name: 'CARE_TARGET_IS_USED',
      message: 'Mục tiêu chăm sóc đã được sử dụng',
    },
    1218: {
      name: 'QUANTITY_INVALID',
      message: 'Số lượng không hợp lệ',
    },
    1219: {
      name: 'USER_HAS_PLAN',
      message: 'Tài khoản đã được gán kế hoạch',
    },
    1220: {
      name: 'PRODUCT_AM_NOT_EXIST',
      message: 'Sản phẩm của AM không tồn tại',
    },
    1221: {
      name: 'DEPARTMENT_HAS_PLAN',
      message: 'Phòng đã được gán kế hoạch',
    },
    1222: {
      name: 'DEPARTMENT_NEST_HAS_PLAN',
      message: 'Tổ đã được gán kế hoạch',
    },
    1223: {
      name: 'NO_PLANS_ASSIGNED',
      message: 'Không có kế hoạch được giao',
    },
    1224: {
      name: 'PLAN_INVALID',
      message: 'Kế hoạch không hợp lệ',
    },
    1225: {
      name: 'TRANSACTION_NOT_EXSIT',
      message: 'Giao dịch không tồn tại',
    },
    1231: {
      name: 'PLAN_APPROVE_UNQUALIFIED',
      message: 'Kế hoạch chưa đủ điều kiện',
    },
    1229: {
      name: 'TARGET_HAS_SUB',
      message: 'Chỉ tiêu đã được gán chỉ tiêu con',
    },
  },
  accessToken: {
    url: 'account/getAccessToken',
    method: 'GET',
  },
  loginCustomer: {
    url: 'account/customer/login',
    method: 'POST',
    responses: {
      CREDENTIALS_INVALID: {
        code: 4,
        message: 'Tên người dùng hoặc mật khẩu không hợp lệ',
      },
    },
  },
  loginArtist: {
    url: '/account/artist/login',
    method: 'POST',
    responses: {
      CREDENTIALS_INVALID: {
        code: 1001,
        message: 'Tên người dùng hoặc mật khẩu không hợp lệ',
      },
    },
  },
  // change pass user
  changePassUser: {
    url: 'account/artist/password/change',
    method: 'POST',
  },
  changePassUser1: {
    url: '/account/customer/password/change',
    method: 'POST',
  },
}

export default APIs
