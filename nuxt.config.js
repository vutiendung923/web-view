import colors from 'vuetify/es5/util/colors'
import URL from './assets/configurations/Base_Url'
import en from './assets/locale/en'
import vi from './assets/locale/vi'
export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  server: {
    port: 10707,
    host: '0.0.0.0',
  },
  head: {
    titleTemplate: '%s Vietnam Artist Booking',
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1,user-scalable=no' },
      {
        hid: 'description',
        name: 'description',
        content:'',
      },
      { name: 'facebook-domain-verification', content: 'xezdnfu8up3sea93wvheulwvpytxr9' }, 
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: 'logo/logo_white.png', },
      {
        rel: 'apple-touch-icon-precomposed',
        type: 'image/x-icon',
        href: 'logo/logo_196.png',
        sizes: '512x512',
      },
      {
        rel: 'apple-touch-icon-precomposed',
        type: 'image/x-icon',
        href: 'logo/logo_196.png',
        sizes: '120x120',
      },
      {
        rel: 'apple-touch-icon-precomposed',
        type: 'image/x-icon',
        href: 'logo/logo_196.png',
        sizes: '125x125',
      },
      {
        rel: 'apple-touch-icon-precomposed',
        type: 'image/x-icon',
        href: 'logo/logo_196.png',
        sizes: '128x128',
      },
      {
        rel: 'apple-touch-icon-precomposed',
        type: 'image/x-icon',
        href: 'logo/logo_196.png',
        sizes: '192x192',
      },
      {
        rel: 'apple-touch-icon',
        type: 'image/x-icon',
        href: 'logo/logo_white.png',
        sizes: '48x48',
      },

      {
        rel: 'stylesheet',
        href:
          // 'https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap',
          'https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap',
      },
    ],
  },



  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['~/assets/main.scss'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/ult.js',
    '~/plugins/APIs.js',
    '~/plugins/axios.js',

    { src: '@/plugins/vue-masonry.js', ssr: false },
    {
      src: '~/plugins/vue-carousel.js',
      mode: 'client',
      ssr: false,
    },
    {
      src: '~/plugins/notification.js',
      ssr: false,
    },
    {
      src: '~/plugins/common.js',
      ssr: false,
    },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    // '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
   
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    'vue-wait/nuxt',
    // '@nuxtjs/proxy'
    // '@nuxtjs/pwa',
    // https://go.nuxtjs.dev/pwa
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: URL.url,
    https:false,
  },
  // proxy: {
  //   'https://vab.xteldev.com/api/': { target: URL.url, pathRewrite: {'^/api/': ''} }
  //   },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  // pwa: {
  //   manifest: {
  //     lang: 'en',
  //   },
  // },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    treeShake: true,
    lang: {
      locales: {
        vi,
        en,
      },
      current: 'vi',
    },
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          // primary: '#2F55ED',
          primary: '#2F55ED',
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
        },
        light: {
          primary: '#2F55ED',
        }
      },
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    /*
     ** You can extend webpack config here
     */
    vendor: ['vue-pdf'],
    extend(config, ctx) {
      config.output.globalObject = 'this'
    },
    transpile: [/^vue2-google-maps($|\/)/],
  },
}
