import Cookies from 'js-cookie'
import uniqid from 'uniqid'
import APIs from '~/assets/configurations/API_Config'

export default (context, inject) => {
  inject('login', async ({ userName, password, ip, accountType }) => {

    const transid = uniqid()
    const response = await context.app.$axios({
      url: accountType ? APIs.loginCustomer.url : APIs.loginArtist.url,
      method: APIs.loginCustomer.method,
      headers: {
        ip,
      },
      data: {
        channel: APIs.channel, 
        transid,
        userName,
        password,
      },
    })
    if (!response.error) {
      context.store.dispatch('notification/set_notifications', {
        type: 'success',
        color: 'success',
        text: 'Đăng nhập thành công',
        dark: true,
      })
    }
    return response
  })
  inject('getAccessToken', async () => {
    const transid = uniqid()
    const response = await context.app.$axios({
      url: APIs.accessToken.url,
      method: APIs.accessToken.method,
      params: {
        channel: APIs.channel,
        transid,
        refreshToken: Cookies.get('refreshToken'),
        role: Cookies.get('role'),
      },
    })
    if (response.error) {
      context.$logout()
      context.store.commit('login/setRole', '')
      context.store.commit('login/setToken', '')
      context.store.commit('login/setAvatar', '')
      context.store.commit('login/setAccountLogin', '')
      context.store.commit('login/setLogin')
      context.store.commit('app/callInfoArtist')
    } else {
      Cookies.set('token', response.data.data)
      context.store.commit('app/callInfoArtist')
    }
    return response
  })
  inject('changePassUser', async (data) => {
    const response = await context.app.$axios({
      url: APIs.changePassUser.url,
      method: APIs.changePassUser.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      data: {
        channel: APIs.channel,
        pass_old: data.pass_old,
        password: data.pass_new,
      },
    })
    return response
  })
  inject('changePassUser1', async (data) => {
    const response = await context.app.$axios({
      url: APIs.changePassUser1.url,
      method: APIs.changePassUser1.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      data: {
        channel: APIs.channel,
        pass_old: data.pass_old,
        password: data.pass_new,
      },
    })
    return response
  })
}
