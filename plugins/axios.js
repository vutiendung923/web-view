import Cookies from 'js-cookie'
import APIs from '~/assets/configurations/API_Config'

export default function (context) {
  context.$axios.onResponse((response) => {
    if (response.data.status === 'OK') {
      return {
        error: false,
        data: response.data,
      }
    } else if (response.data.status === 'ZERO_RESULTS') {
      context.store.dispatch('notification/set_notifications', {
        type: 'warning',
        color: 'white',
        text: 'Không tìm thấy địa chỉ',
        dark: true,
      })
      return {
        error: true,
        data: response.data,
      }
    }
    const code = response.data.error.code
    const OK = APIs.responses.OK.code
    if (
      code === APIs.responses.TOKEN_EXPIRE.code ||
      code === APIs.responses.TOKEN_INVALID.code
    ) {
      if (Cookies.get('refreshToken')) {
        context.app.store.$getAccessToken()
      }
    } else if (code === OK) {
      return {
        error: false,
        data: response.data,
      }
    } else {
    
      context.app.router.app.$log.warn('Có lỗi khi gọi API: ', {
        response,
      })
      context.store.dispatch('notification/set_notifications', {
        type: 'warning',
        dark: false,
        text: response.data.error.message,
      })
      return {
        error: true,
        data: response.data,
      }
    }
  })

  context.$axios.onError((error) => {
    context.app.router.app.$log.error('Có lỗi/exception: ', {
      error,
      response: error.response,
    })
    return {
      error: true,
    }
  })
}
