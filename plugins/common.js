import Vue from 'vue'
import VueLogger from 'vuejs-logger'
import Notifications from 'vue-notification'
import * as VueGoogleMaps from 'vue2-google-maps'
import InfiniteLoading from 'vue-infinite-loading'
import 'viewerjs/dist/viewer.css'
import Viewer from 'v-viewer'
import GOOGLE from '~/assets/configurations/GOOGLE_MAP_API_KEY'
import ProgressCircular from '~/components/ProgressCircular.vue'
const isProduction = process.env.NODE_ENV === 'production'
const options = {
  isEnabled: true,
  logLevel: isProduction ? 'error' : 'debug',
  stringifyArguments: false,
  showLogLevel: true,
  showMethodName: true,
  separator: '|',
  showConsoleColors: true,
}
Vue.use(Viewer)
Vue.use(InfiniteLoading, {
  distance: 0,
  slots: {
    noMore: 'Đã tải hết dữ liệu', // you can pass a string value
    noResults: 'Không còn dữ liệu',
    spinner: ProgressCircular,
    // error: InfiniteError, // you also can pass a Vue component as a slot
  },
})
Vue.use(VueGoogleMaps, {
  load: {
    key: GOOGLE.API_KEY_MAP,
    libraries: 'places',
    region: 'VI',
    language: 'vi',
  },
})
Vue.use(VueLogger, options)
Vue.use(Notifications)
