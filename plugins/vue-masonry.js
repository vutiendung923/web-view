import { Freewall } from 'freewall'
import Vue from 'vue'
import isotope from 'vueisotope'

Vue.component('Isotope', isotope)

export default (_, inject) => {
  inject('Freewall', Freewall)
}
