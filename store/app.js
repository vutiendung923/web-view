export const state = () => ({
  viewPort: '',
  dataLanguage: 1,
  alert_display_time: 6000,
  callInfo: false,
  paymentSelected: 0,
})
export const mutations = {
  setViewPort(state, viewPort) {
    state.viewPort = viewPort
  },
  callInfoArtist(state) {
    state.callInfo = !state.callInfo
  },
  setLanguage(state, language) {
    state.dataLanguage = language
  },
  set_time_alert(state, payload) {
    state.alert_display_time = Number(payload)
  },


  CHANGE_STATE(state, payload) {
    state.paymentSelected = payload.ccc
  },

  SET_PAYMENT_CHANNEL(state, payment_obj) {
    state.paymentSelected = payment_obj.id
    state.paymentSelectedObj = payment_obj
  },

}
