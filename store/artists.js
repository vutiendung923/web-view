import uniqid from 'uniqid'
import Cookies from 'js-cookie'
import APIs from '~/assets/configurations/API_Config.js'

export const state = () => {
  return {
    listArtists: {
      url: 'artist',
      method: 'GET',
    },
    // đăng lý khách hàng
    accountRegister: {
      url: 'account/customerRegister',
      method: 'POST'
    },
    artistRegister: {
      url: 'account/artistRegister',
      method: 'POST'
    },
    listField: {
      url: 'field',
      method: 'GET'
    },
    Qualification: {
      url: 'qualification',
      method: 'GET'
    },
    MusicType: {
      url: 'musicType',
      method: 'GET',
    },
    tabs: false,
  }
}
export const mutations = {
  setTabs(state) {
    state.tabs = !state.tabs
  },
}
export const actions = {
  
  async MusicType(vueContext, data) {
    const transid = uniqid()
      const response = await this.$axios({
        url: vueContext.state.MusicType.url,
        method: vueContext.state.MusicType.method,

        params: {
          channel: APIs.channel,
          transid,
          ...data,
        },
      })
      return response
    
  },
  async Qualification(vueContext, data) {
    const transid = uniqid()
      const response = await this.$axios({
        url: vueContext.state.Qualification.url,
        method: vueContext.state.Qualification.method,

        params: {
          channel: APIs.channel,
          transid,
          ...data,
        },
      })
      return response
    
  },
  async listField(vueContext, data) {
    const transid = uniqid()
      const response = await this.$axios({
        url: vueContext.state.listField.url,
        method: vueContext.state.listField.method,

        params: {
          channel: APIs.channel,
          transid,
          ...data,
        },
      })
      return response
    
  },
  async accountRegister(vueContext, data) {
    const transid = uniqid()
      const response = await this.$axios({
        url: vueContext.state.accountRegister.url,
        method: vueContext.state.accountRegister.method,

        data: {
          channel: APIs.channel,
          transid,
          ...data,
        },
      })
      return response
    
  },
  async artistRegister(vueContext, data) {
    const transid = uniqid()
      const response = await this.$axios({
        url: vueContext.state.artistRegister.url,
        method: vueContext.state.artistRegister.method,

        data: {
          channel: APIs.channel,
          transid,
          ...data,
        },
      })
      return response
    
  },

  async listArtists(vueContext, data) {
    const transid = uniqid()
    if (Cookies.get('token')) {
      const response = await this.$axios({
        url: vueContext.state.listArtists.url,
        method: vueContext.state.listArtists.method,
        headers: {
          Authorization: 'Bearer ' + Cookies.get('token'),
        },
        params: {
          channel: APIs.channel,
          transid,
          ...data,
        },
      })
      return response
    } else {
      const response = await this.$axios({
        url: vueContext.state.listArtists.url,
        method: vueContext.state.listArtists.method,

        params: {
          channel: APIs.channel,
          transid,
          ...data,
        },
      })
      return response
    }
  },
}
