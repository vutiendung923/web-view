import uniqid from 'uniqid'
import Cookies from 'js-cookie'
import APIs from '~/assets/configurations/API_Config.js'

export const state = () => {
  return {
    customerBooking: {
      url: 'booking/customer/request',
      method: 'POST',
    },
    artistsInformation: {
      url: 'booking/response',
      method: 'GET',
    },
    BookingHistoryList: {
      url: 'booking/booked',
      method: 'GET',
    },
    confirmBooking: {
      url: 'booking/artist/verify',
      method: 'POST',
    },
    updateLichDien: {
      url: 'booking/artist/perform',
      method: 'POST',
    },
    level: {
      url: 'artist/dictionary',
      method: 'GET',
    },
    // đánh giá sao
    bookingRating: {
      url:'booking/artist/rating',
      method:'POST'
    },
    // danh sách hình thức đặt cọc
    formalityDeposit: {
      url: 'common/formalityDeposit',
      method: 'GET'
    },

    // cổng thanh toán
    get_payment_gw_list: {
      url: 'payment',
      method: 'GET'
    },
    // lấy số tiền yêu cầu đặt cọc của nghệ sĩ khi booking
    bookingArtistMoneyDeposit: {
      url: 'booking/artist/moneyDeposit',
      method: 'GET'
    },
    // đặt cọc theo booking
    listDeposit: {
      url: 'deposit',
      method: 'GET'
    },
    depositAdd: {
      url: 'deposit/add',
      method: 'POST'
    },
    // common/bank
    commonBank: {
      url: 'common/bank',
      method: 'GET'
    },
    depositDetail: {
      url: 'deposit/detail',
      method: 'GET'
    }

  }
}
export const actions = {
  

  async depositDetail(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.depositDetail.url,
      method: vueContext.state.depositDetail.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      params: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  async commonBank(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.commonBank.url,
      method: vueContext.state.commonBank.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      params: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },

  async depositAdd(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.depositAdd.url,
      method: vueContext.state.depositAdd.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      data: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  async listDeposit(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.listDeposit.url,
      method: vueContext.state.listDeposit.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      params: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  async bookingArtistMoneyDeposit(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.bookingArtistMoneyDeposit.url,
      method: vueContext.state.bookingArtistMoneyDeposit.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      params: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },

  async get_payment_gw_list(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.get_payment_gw_list.url,
      method: vueContext.state.get_payment_gw_list.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      params: {
        channel: APIs.channel,
        salesChannelCode: 'VAB_WEB',
        transid,
        ...data,
      },
    })
    return response
  },

  


  async formalityDeposit(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.formalityDeposit.url,
      method: vueContext.state.formalityDeposit.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      params: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  async level(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.level.url,
      method: vueContext.state.level.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      params: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  
  async bookingRating(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.bookingRating.url,
      method: vueContext.state.bookingRating.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      data: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  async updateLichDien(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.updateLichDien.url,
      method: vueContext.state.updateLichDien.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      data: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  async customerBooking(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.customerBooking.url,
      method: vueContext.state.customerBooking.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      data: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  async artistsInformation(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.artistsInformation.url,
      method: vueContext.state.artistsInformation.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      params: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  async BookingHistoryList(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.BookingHistoryList.url,
      method: vueContext.state.BookingHistoryList.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      params: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  async confirmBooking(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.confirmBooking.url,
      method: vueContext.state.confirmBooking.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      data: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
}
