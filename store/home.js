import uniqid from 'uniqid'
import APIs from '~/assets/configurations/API_Config.js'

export const state = () => {
  return {
    news: {
      url: '/post',
      method: 'GET',
    },
    listDoiTac: {
      url: '/home/partner',
      method: 'GET',
    },
    listBanner: {
      url: '/home/banner',
      method: 'GET',
    },
    listSpHot: {
      url: 'home/product',
      method: 'GET',
    },
  }
}
export const actions = {
  async listSpHot(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.listSpHot.url,
      method: vueContext.state.listSpHot.method,
       progress: false ,
      params: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  async listBanner(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.listBanner.url,
      method: vueContext.state.listBanner.method,
      header: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
        "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token",
        "Access-Control-Allow-Credentials":"true"
      },
      params: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  async listDoiTac(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.listDoiTac.url,
      method: vueContext.state.listDoiTac.method,

      params: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  async news(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.news.url,
      method: vueContext.state.news.method,
      params: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
}
