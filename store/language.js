import uniqid from 'uniqid'
// import Cookies from 'js-cookie'
import APIs from '~/assets/configurations/API_Config.js'

export const state = () => {
  return {
    language: {
      url: 'language',
      method: 'GET',
    },
  }
}
export const actions = {
  async language(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.language.url,
      method: vueContext.state.language.method,
      // headers: {
      //   Authorization: ' Bearer ' + Cookies.get('token'),
      // },
      params: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
}
