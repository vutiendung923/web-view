import Cookies from 'js-cookie'
import uniqid from 'uniqid'
import APIs from '~/assets/configurations/API_Config'
export const state = () => ({
  detailProfile: {
    url: 'artist/profile',
    method: 'GET',
  },
  getDetail: {
     url: 'customer/detail',
     method: 'GET'
  },
  username: {
    errors: [],
    value: '',
  },
  password: {
    errors: [],
    value: '',
  },
  url: '',
  role: [],
  info: {},
  accountType: true,
  openLogin: false,
  openPayment: false,
  openOnlinePayment: false,
  openTransferPayment: false,
  openAddDeposit: false,
  avatar: '',
  token: '',
  accountLogin: '',
})
export const mutations = {
  setAvatar(state, payload) {
    state.avatar = payload
  },
  setAccountLogin(state, payload) {
    state.accountLogin = payload
  },
  setToken(state, payload) {
    state.token = payload
  },
  Seturl(state, payload) {
    state.url = payload
  },
  setRole(state, payload) {
    state.role = payload
  },
  setLogin(state) {
    state.openLogin = !state.openLogin
  },
// hinh thức thanh toán
  setPayment(state) {
    state.openPayment = !state.openPayment  
  },
  // thanh toán online
  setOnlinePayment(state) {
    state.openOnlinePayment = !state.openOnlinePayment
   },
  // thanh toán qua hình thức chuyển khoản
  setTransferPayment(state) {
    state.openTransferPayment = !state.openTransferPayment 
  },
  setAddDeposit(state) {
    state.openAddDeposit = !state.openAddDeposit 
  },


  setUsername(state, payload) {
    state.username = {
      ...state.username,
      value: payload,
      errors: [],
    }
  },
  setDataInfo(state, payload) {
    state.info = payload
  },
  setAccount(state, payload) {
    state.accountType = payload
  },
  setPassword(state, payload) {
    state.password = {
      ...state.password,
      value: payload,
      errors: [],
    }
  },
  unsetItem(state) {
    state.username.value = ''
    state.password.value = ''
  },
  usernameHasErrors(state, payload) {
    state.username = {
      ...state.username,
      errors: [payload],
    }
  },
  passwordHasErrors(state, payload) {
    state.password = {
      ...state.password,
      errors: [payload],
    }
  },
  clearErrors(state, payload) {
    state.username = {
      ...state.username,
      errors: [],
    }
    state.password = {
      ...state.password,
      errors: [],
    }
  },
}
export const actions = {
  setUsername(vueContext, payload) {
    vueContext.commit('setUsername', payload)
  },

  setPassword(vueContext, payload) {
    vueContext.commit('setPassword', payload)
  },

  
  async getDetail(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.getDetail.url,
      method: vueContext.state.getDetail.method,
      headers: {
        Authorization: Cookies.get('token')
          ? 'Bearer ' + Cookies.get('token')
          : '',
      },
      params: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  async detailProfile(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.detailProfile.url,
      method: vueContext.state.detailProfile.method,
      headers: {
        Authorization: Cookies.get('token')
          ? 'Bearer ' + Cookies.get('token')
          : '',
      },
      params: {
        langId: vueContext.rootState.app.dataLanguage,
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    vueContext.commit('setRole', response.data.data.roles)
    return response
  },
  Submit(vueContext, payload) {
    const userName = vueContext.state.username.value.toLowerCase()
    const password = vueContext.state.password.value.toLowerCase()
    const ip = payload
    if (userName.includes(' ')) {
      vueContext.commit('usernameHasErrors', 'Tên người dùng không hợp lệ.')
      this.app.wait.end('logging')
      return
    }
    vueContext.commit('clearErrors')
    // Start calling API
    this.$login({
      userName,
      password,
      ip,
      accountType: vueContext.state.accountType,
    })
      .then((response) => {
        const errorCode = response.data.error.code
        const data = response.data.data
        if (errorCode === Number(APIs.responses.OK.code)) {
          Cookies.set('dataInfo', JSON.stringify(data))
          Cookies.set('accountId', this.$isNullOrEmpty(data.account.artistId) ? data.account.id : data.account.artistId )
          Cookies.set('token', data.accessToken)
          Cookies.set('avatar', data.account.avatar)
          Cookies.set('account', this.$isNullOrEmpty(data.account.artistName) ? data.account.fullName :  data.account.artistName)
          Cookies.set('role', data.role)
          Cookies.set('refreshToken', data.refreshToken)
          Cookies.set('email', data.account.email)
          Cookies.set('approvalStatus', data.account.approvalStatus)
          vueContext.commit('setDataInfo', data)
          vueContext.commit('setToken', data.accessToken)
          vueContext.commit('setAccountLogin', this.$isNullOrEmpty(data.account.artistName) ? data.account.fullName :  data.account.artistName )
          vueContext.commit('setAvatar', data.account.avatar)
          vueContext.commit('setLogin')
          vueContext.commit('setRole', [data.role])
          if (
            vueContext.state.url.name === 'booking' &&
            data.role === 'ARTIST'
          ) {
            vueContext.dispatch('detailProfile', {
              artistId: data.account.artistId,
            })
          }
          if (data.account.artistId) {
            let plan = Cookies.get('accountId')
            this.$router.push( {path: `/booking/${this.$removeAccents(Cookies.get('account')).replace(
              / /g,
              '-'
            )}`,
            query: { plan },
           })
          }
        }
        this.app.wait.end('logging')
      })
      .catch((errors) => {
        this.app.wait.end('logging')
      })
  },
}
