import uniqid from 'uniqid'
import Cookies from 'js-cookie'
import APIs from '~/assets/configurations/API_Config.js'
import baseURL from '~/assets/configurations/Base_Url.js'

export const state = () => {
  return {
    deleteBooking: {
      url: 'booking/customer/deny',
      method: 'GET',
    },
    uploadFile: {
      method: 'POST',
    },
    updateEmbedded: {
      url: 'artist/socialEmbedded/update',
      method: 'POST',
    },
    favorite: {
      url: 'artist/favorite',
      method: 'GET',
    },
    unFavorite: {
      url: 'artist/unFavorite',
      method: 'GET',
    },
    addStory: {
      url: 'artist/story/add',
      method: 'POST',
    },
    updateStory: {
      url: 'artist/story/update',
      method: 'POST',
    },
    deleteStory: {
      url: 'artist/story/delete',
      method: 'GET',
    },
    addProduct: {
      url: 'artist/product/add',
      method: 'POST',
    },
    productHot: {
      url: 'artist/product/push',
      method: 'POST',
    },
    updateProduct: {
      url: 'artist/product/update',
      method: 'POST',
    },
    deleteProduct: {
      url: 'artist/product/delete',
      method: 'GET',
    },
    bookingProduct: {
      url: 'customer/booking',
      method: 'POST',
    },
    updateBanner: {
      url: 'artist/banner/update',
      method: 'POST',
    },
    addBanner: {
      url: 'artist/banner/add',
      method: 'POST',
    },
    deleteBanner: {
      url: 'artist/banner/delete',
      method: 'GET',
    },
    comfirmBooking: {
      url: '/booking/customer/verify',
      method: 'POST',
    },
    updateProfile: {
      url: '/artist/description/update',
      method: 'POST',
    },
  }
}
export const actions = {
  async updateProfile(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.updateProfile.url,
      method: vueContext.state.updateProfile.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      data: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  async comfirmBooking(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.comfirmBooking.url,
      method: vueContext.state.comfirmBooking.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      data: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  async productHot(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.productHot.url,
      method: vueContext.state.productHot.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      data: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  async bookingProduct(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.bookingProduct.url,
      method: vueContext.state.bookingProduct.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      data: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },

  async deleteBooking(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.deleteBooking.url,
      method: vueContext.state.deleteBooking.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      params: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  async uploadFile(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      baseURL: baseURL.uploadAvatar,
      method: vueContext.state.uploadFile.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      // params: {
      //   transid,
      //   channel: APIs.channel,
      //   folder_dir: data.folder,
      // },
      data: data.formData,
    })
    return response
  },
  async favorite(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.favorite.url,
      method: vueContext.state.favorite.method,
      headers: {
        Authorization: Cookies.get('token')
          ? 'Bearer ' + Cookies.get('token')
          : '',
      },
      params: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  async unFavorite(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.unFavorite.url,
      method: vueContext.state.unFavorite.method,
      headers: {
        Authorization: Cookies.get('token')
          ? 'Bearer ' + Cookies.get('token')
          : '',
      },
      params: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  async addProduct(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.addProduct.url,
      method: vueContext.state.addProduct.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      data: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  async deleteProduct(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.deleteProduct.url,
      method: vueContext.state.deleteProduct.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      params: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  async deleteBanner(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.deleteBanner.url,
      method: vueContext.state.deleteBanner.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      params: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  async addBanner(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.addBanner.url,
      method: vueContext.state.addBanner.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      data: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  async updateBanner(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.updateBanner.url,
      method: vueContext.state.updateBanner.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      data: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  async updateProduct(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.updateProduct.url,
      method: vueContext.state.updateProduct.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      data: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  async updateEmbedded(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.updateEmbedded.url,
      method: vueContext.state.updateEmbedded.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      data: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  async addStory(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.addStory.url,
      method: vueContext.state.addStory.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      data: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  async updateStory(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.updateStory.url,
      method: vueContext.state.updateStory.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      data: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
  async deleteStory(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.deleteStory.url,
      method: vueContext.state.deleteStory.method,
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      },
      params: {
        channel: APIs.channel,
        transid,
        ...data,
      },
    })
    return response
  },
}
