import uniqid from 'uniqid'
import APIs from '~/assets/configurations/API_Config.js'

export const state = () => {
  return {
    getFooter: {
      url: 'config/webPage',
      method: 'GET',
    },
    detailFooter: {
      url: 'config/staticPage',
      method: 'GET',
    },
    getInformation: {
      url: 'information',
      method: 'GET',
    },
    getNews: {
      url: 'post/detail',
      method: 'GET',
    },
    page: true,
  }
}
export const mutations = {
  setPage(state) {
    state.page = !state.page
  },
}
export const actions = {
  async getNews(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.getNews.url,
      method: vueContext.state.getNews.method,
      params: {
        channel: APIs.channel,
        transid,
        id: 1,
        langId: vueContext.rootState.app.dataLanguage,
        ...data,
      },
    })
    return response
  },
  async getInformation(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.getInformation.url,
      method: vueContext.state.getInformation.method,
      params: {
        channel: APIs.channel,
        transid,
        id: 1,
        langId: vueContext.rootState.app.dataLanguage,
        ...data,
      },
    })
    return response
  },
  async getFooter(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.getFooter.url,
      method: vueContext.state.getFooter.method,
      params: {
        channel: APIs.channel,
        transid,
        langId: vueContext.rootState.app.dataLanguage,
        ...data,
      },
    })
    return response
  },
  async detailFooter(vueContext, data) {
    const transid = uniqid()
    const response = await this.$axios({
      url: vueContext.state.detailFooter.url,
      method: vueContext.state.detailFooter.method,
      params: {
        channel: APIs.channel,
        transid,
        langId: vueContext.rootState.app.dataLanguage,
        ...data,
      },
    })
    return response
  },
}
